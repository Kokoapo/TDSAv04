import Vue from 'vue'
import Router from 'vue-router'
import Menu from '@/components/Menu'
import Fatorial from '@/components/Fatorial'
import Potencia2 from '@/components/Pow2'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Menu',
      component: Menu
    },
    {
      path: '/fatorial',
      name: 'Fatorial',
      component: Fatorial
    },
    {
      path: '/potencia2',
      name: 'Potencia2',
      component: Potencia2
    }
  ]
})
